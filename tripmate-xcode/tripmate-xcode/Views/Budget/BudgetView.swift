import SwiftUI

struct BudgetView: View {
    @EnvironmentObject var dataStore: DataStore
    @State var isPresentingBudgetForm: Bool = false
    @State var newBudgetFormData = Budget.FormData()
    @State var isPresentingStartBudget: Bool = false
    @State var startBudget = StartingBudget.FormData()
    
    var body: some View {
        NavigationStack {
            VStack {
                HStack{
                    Image("Budget image").resizable().frame(width:180, height:150)
                    
                    VStack{
                        Text("Total Budget Remaining:")
                        Text("$" + String(format: "%05.2f", dataStore.calculateRemainingBudget()))
                            .font(.title).foregroundColor(Color(red: 0.045, green: 0.207, blue: 0.455))
                        Text("Starting budget: $" + String(format: "%05.2f", dataStore.updateStartingBudget()))
                        
                        Button("Adjust") {
                            isPresentingStartBudget = true
                        }
                        .frame(maxWidth: 95)
                        .frame(height: 30)
                        .background(Color(red: 0.045, green: 0.207, blue: 0.455))
                        .cornerRadius(30)
                        .foregroundColor(.white)
                        .padding(2)
                        .sheet(isPresented: $isPresentingStartBudget) {
                            Button("Save") {
                                let updateStart = StartingBudget.create(from: startBudget)
                                dataStore.addStartingBudget(updateStart)
                                newBudgetFormData = Budget.FormData()
                                isPresentingStartBudget.toggle()
                            }.padding()
                            VStack{
                                StartingBudgetForm(data: $startBudget)
                            }
                            .presentationDetents([.medium])
                        }
                    }
                }.padding(.top)
                
                List(dataStore.budgets) { budget in
                    HStack {
                        Text(budget.description + " (" + budget.budgetType.rawValue + ")").padding(-1.6)
                        ZStack {
                            Divider()
                        }
                        if let cost = budget.domesticCostNum {
                            Text("$" + String(format: "%05.2f", cost))
                        }
                    }
                    .swipeActions(edge: .trailing) {
                        Button(role: .destructive) {
                            dataStore.removeExpense(budget)
                        } label: {
                            Label("Delete", systemImage: "trash")
                        }.frame(height: 250)
                    }
                    
                }
                
                VStack {
                    NavigationLink(destination: BudgetBreakdownView()){
                        Text("Summary")
                    }
                    .frame(maxWidth: 120)
                    .frame(height: 30)
                    .background(Color(red: 0.045, green: 0.207, blue: 0.455))
                    .cornerRadius(30)
                    .foregroundColor(.white)
                    .padding(2)
                    Text("Total Spent: $" + String(dataStore.calculateTotalSpent()))
                        .bold()
                    ZStack {
                        Divider()
                    }
                }
            }
            .navigationTitle("Budget")
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button("Add") { isPresentingBudgetForm.toggle() }
                }
            }
            .sheet(isPresented: $isPresentingBudgetForm) {
                NavigationStack {
                    BudgetForm(data: $newBudgetFormData)
                        .toolbar {
                            ToolbarItem(placement: .navigationBarLeading) {
                                Button("Cancel") {
                                    isPresentingBudgetForm = false
                                    newBudgetFormData = Budget.FormData()
                                }
                            }
                            ToolbarItem(placement: .navigationBarTrailing) {
                                Button("Save") {
                                    let newBudget = Budget.create(from: newBudgetFormData)
                                    dataStore.addToBudget(newBudget)
                                    newBudgetFormData = Budget.FormData()
                                    isPresentingBudgetForm = false
                                }
                            }
                        }
                }
                .padding()
            }
        }
    }
}

struct BudgetView_Previews: PreviewProvider {
    static var previews: some View {
        BudgetView()
            .environmentObject(DataStore())
    }
}
