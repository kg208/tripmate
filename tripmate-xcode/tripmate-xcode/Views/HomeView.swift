import SwiftUI

struct HomeView: View {
    @EnvironmentObject var dataStore: DataStore
    
    var body: some View {
        VStack{
            Text("Welcome Back!").font(.title).bold()
            Image("Home image")
                .resizable()
                .scaledToFit()
                .aspectRatio(contentMode: .fit)
            Spacer()
            VStack {
                Text("Upcoming").font(.title2).bold().padding()
                List {
                    Section(header: Text(dataStore.findHomeDate())) {
                        ForEach (dataStore.findHomeItinerary()) { activity in
                            HomeDetail(activity: activity)
                        }
                    }.headerProminence(.increased)
                }
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(Color(red: 0.949, green: 0.949, blue: 0.949))
            ZStack {
                Divider()
            }
        }
    }
}

struct HomeDetail: View {
    @State var activity: Itinerary
    
    var body: some View {
        HStack {
            Text(activity.timeString)
                .font(.title2)
                .bold()
            VStack(alignment: .leading) {
                Text(activity.activityType.rawValue.uppercased())
                Text(activity.title)
                    .font(.title2)
                    .bold()
                HStack {
                    Image(systemName: "note.text")
                        .font(.title2)
                    Text(activity.notes)
                }
            }
        }
    }
}


struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
            .environmentObject(DataStore())
    }
}
