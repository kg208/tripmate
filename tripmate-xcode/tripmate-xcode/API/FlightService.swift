import Foundation

protocol FlightAPI {
    func fetchFlight(flightNumber: String) async throws -> FlightResponse
}

struct FlightAPIClient: FlightAPI, APIClient {
    let session: URLSession = .shared
    
    func fetchFlight(flightNumber: String) async throws -> FlightResponse {
        let path = FlightEndpoint.path(flightNumber: flightNumber)
        let headers = [
            ("X-RapidAPI-Key", "0996270052mshf3e5099116b565cp1633b0jsn782a07b1c951"),
            ("X-RapidAPI-Host", "flight-radar1.p.rapidapi.com")
        ]
        let result: FlightResponse = try await performRequestHeader(url: path, headers: headers)
        return result
    }
}

struct MockFlightAPIClient: FlightAPI {
    func fetchFlight(flightNumber: String) async throws -> FlightResponse {
        return FlightResponse.mock()
    }
}
