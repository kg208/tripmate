import SwiftUI

struct ItineraryForm: View {
    @Binding var data: Itinerary.FormData
    
    var body: some View {
        Form {
            TextFieldWithLabel(label: "Activity Title", text: $data.title, prompt: "Title")
            DatePicker("Date", selection: $data.date)
            Picker(selection: $data.activityType, label: Text("Activity Type")) {
                ForEach(Itinerary.ActivityType.allCases) { activityType in
                    Text(activityType.rawValue)
                }
            }
            .pickerStyle(.menu)
            TextFieldWithLabel(label: "Additional Notes", text: $data.notes, prompt: "Notes")
        }
    }
}

struct TextFieldWithLabel: View {
    let label: String
    @Binding var text: String
    var prompt: String? = nil
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(label)
                .bold()
                .font(.caption)
            TextField(label, text: $text, prompt: prompt != nil ? Text(prompt!) : nil)
                .padding(.bottom, 20)
                .disableAutocorrection(true)
        }
    }
}

struct ItineraryForm_Previews: PreviewProvider {
    static var previews: some View {
        ItineraryForm(data: Binding.constant(Itinerary.FormData()))
    }
}
