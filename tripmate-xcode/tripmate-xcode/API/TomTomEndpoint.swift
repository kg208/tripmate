import Foundation

// valid URL: https://api.tomtom.com/search/2/poiSearch/ocean.json?lat=34.0356&lon=-118.5156&view=Unified&key=vVkUGcvP8y8LF3RWwts6joxFmMUGhCxC

struct TomTomEndpoint {
    static let baseUrl = "https://api.tomtom.com/search/2/poiSearch"
    static let apiKey = "vVkUGcvP8y8LF3RWwts6joxFmMUGhCxC"
    
    static func path(type: String, latitude: Float, longitude: Float) -> String {
        let url = "\(baseUrl)/\(type)" + ".json"
        let location = "lat=\(latitude)" + "&lon=\(longitude)"
        let view = "view=Unified"
        let key = "key=\(apiKey)"
        return "\(url)?\(location)&\(view)&\(key)"
    }
}
