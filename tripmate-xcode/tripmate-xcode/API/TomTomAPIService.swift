import Foundation

protocol TomTomAPI {
    func fetchPOIs(type: String, latitude: Float, longitude: Float) async throws -> POIsResponse
}

struct TomTomAPIClient: TomTomAPI, APIClient {
    let session: URLSession = .shared
    
    func fetchPOIs(type: String, latitude: Float, longitude: Float) async throws -> POIsResponse {
        let path = TomTomEndpoint.path(type: type, latitude: latitude, longitude: longitude)
        let response: POIsResponse = try await performRequest(url: path)
        return response
    }
}

struct MockTomTomAPIClient: TomTomAPI {
    func fetchPOIs(type: String, latitude: Float, longitude: Float) async throws -> POIsResponse {
        return POIsResponse.mock()
    }
    
}
