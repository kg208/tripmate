import Foundation

struct FlightEndpoint {
    static let startUrl = "https://flight-radar1.p.rapidapi.com/flights/get-more-info?"
    static let endUrl = "&fetchBy=flight&page=1&limit=1"

    static func path(flightNumber: String) -> String {
        return "\(startUrl)query=\(flightNumber)\(endUrl)"
    }
}
