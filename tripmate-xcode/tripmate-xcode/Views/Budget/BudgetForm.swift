import SwiftUI

struct BudgetForm: View {
    @Binding var data: Budget.FormData
    
    var body: some View {
        Form {
            TextFieldWithLabel(label: "Description of Expense", text: $data.description, prompt: "Description")
            Picker(selection: $data.budgetType, label: Text("Budget Type")) {
                ForEach(Budget.BudgetType.allCases) { budgetType in
                    Text(budgetType.rawValue)
                }
            }
            .pickerStyle(.menu)
            TextFieldWithLabel(label: "Cost in Foreign Currency", text: $data.foreignCost, prompt: "Cost")
            TextFieldWithLabel(label: "Exchange Rate ($1 = ?)", text: $data.conversion, prompt: "Conversion")
        }
    }
}

struct BudgetForm_Previews: PreviewProvider {
    static var previews: some View {
        BudgetForm(data: Binding.constant(Budget.FormData()))
    }
}
