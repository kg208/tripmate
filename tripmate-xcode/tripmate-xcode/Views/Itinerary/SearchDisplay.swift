import SwiftUI

struct SearchDisplay: View {
    @EnvironmentObject var dataStore: DataStore
    @EnvironmentObject var searchLoader: SearchLoader
    let results: [Point]
    @State var newItineraryFormData = Itinerary.FormData()
    @State var isPresentingItineraryForm: Bool = false
    
    var body: some View {
        VStack(alignment: .leading) {
            ForEach(results) { point in
                VStack (alignment: .leading) {
                    HStack {
                        VStack (alignment: .leading) {
                            Text(point.poi.name)
                                .bold()
                            Text(point.address.freeformAddress)
                        }
                        Spacer()
                        Button(action: {
                            newItineraryFormData.title = point.poi.name
                            newItineraryFormData.notes = "Address: " + point.address.freeformAddress
                            isPresentingItineraryForm.toggle()
                        } ) {
                            Image(systemName: "plus")
                                .resizable()
                                .padding(6)
                                .frame(width: 24, height: 24)
                                .background(Color(red: 0.045, green: 0.207, blue: 0.455))
                                .clipShape(Circle())
                                .foregroundColor(.white)
                        }
                    }
                }.padding(10)
                Divider()
            }
        }
        .sheet(isPresented: $isPresentingItineraryForm) {
            NavigationStack {
                ItineraryForm(data: $newItineraryFormData)
                    .toolbar {
                        ToolbarItem(placement: .navigationBarLeading) {
                            Button("Cancel") {
                                newItineraryFormData = Itinerary.FormData()
                                isPresentingItineraryForm = false
                            }
                        }
                        ToolbarItem(placement: .navigationBarTrailing) {
                            Button("Save") {
                                let newItinerary = Itinerary.create(from: newItineraryFormData)
                                dataStore.addToActivities(newItinerary)
                                newItineraryFormData = Itinerary.FormData()
                                isPresentingItineraryForm = false
                            }
                        }
                    }
            }
            .padding()
        }
    }
}

struct SearchDisplay_Previews: PreviewProvider {
    static var previews: some View {
        SearchDisplay(results: [Point(id: "yE6tLH7jlKXBls8NHcUt2A", poi: POI(name: "Ocean Avenue"), address: Address(freeformAddress: "Ocean Avenue, Santa Monica, CA 90401"))])
            .environmentObject(DataStore())
            .environmentObject(SearchLoader(apiClient: MockTomTomAPIClient()))
    }
}
