import Foundation

struct Flight: Identifiable, Decodable {
    var id: UUID = UUID()
    var flightNumber: String
    var airline: String
    var origin: String
    var originCode: String
    var destination: String
    var destinationCode: String
    var scheduledDeparture: Int
    var scheduledArrival: Int
    
    struct FormData {
        var flightNumber: String = ""
    }
    
    var dataForForm: FormData {
        FormData(
            flightNumber: flightNumber
        )
    }
}

extension Flight {
    static let previewData = [
        Flight(flightNumber: "UA875", airline: "United Airlines", origin: "San Francisco International Airport", originCode: "SFO", destination: "Tokyo Haneda International Airport", destinationCode: "HND", scheduledDeparture: 1681407900, scheduledArrival: 1681448100)
    ]
}

public struct FlightResponse: Decodable {
    var result: Result
    
    static func mock() -> FlightResponse {
        FlightResponse(result: Result(response: Response(item: Item(current: 1), timestamp: 1681448100, data: [Data(airline: Airline(name: "United Airlines"), airport: Airport(origin: Origin(name: "San Francisco International Airport", code: Code(iata: "SFO"), position: Position(region: Region(city: "San Francisco"))), destination: Destination(name: "Tokyo Haneda International Airport", code: Code(iata: "HND"), position: Position(region: Region(city: "Tokyo")))), time: Time(scheduled: Schedule(departure: 1681926300, arrival: 1681966500)))])))
    }
}

// All of the following structs are for FlightResponse

struct Result: Decodable {
    var response: Response
}

struct Response: Identifiable, Decodable {
    var id: Int {
        timestamp
    }
    var item: Item
    var timestamp: Int
    var data: [Data]
}

struct Item: Decodable {
    var current: Int
}

struct Data: Decodable {
    var airline: Airline
    var airport: Airport
    var time: Time
}

struct Time: Decodable {
    var scheduled: Schedule
}

struct Schedule: Decodable {
    var departure: Int
    var arrival: Int
}

struct Airline: Decodable {
    var name: String
}

struct Airport: Decodable {
    var origin: Origin
    var destination: Destination
}

struct Destination: Decodable {
    var name: String
    var code: Code
    var position: Position
}

struct Origin: Decodable {
    var name: String
    var code: Code
    var position: Position
}

struct Position: Decodable {
    var region: Region
}

struct Region: Decodable {
    var city: String
}

struct Code: Decodable {
    // iata = LAX, for instance
    var iata: String
}
