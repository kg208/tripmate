import SwiftUI

struct FlightView: View {
    @EnvironmentObject var dataStore: DataStore
    @EnvironmentObject var flightLoader: FlightLoader
    @State var isPresentingForm: Bool = false
    @State var newFlightFormData = Flight.FormData()
    
    var body: some View {
        VStack {
            NavigationStack {
                VStack {
                    List{
                        ForEach(dataStore.flights) { flight in
                            FlightContainer(origin: flight.origin, originCode: flight.originCode, destination: flight.destination,
                                            destinationCode:
                                                flight.destinationCode,
                                            scheduledDeparture:
                                                flight.scheduledDeparture,
                                            scheduledArrival:
                                                flight.scheduledArrival)
                            .listRowSeparator(.hidden)
                            .listRowInsets(EdgeInsets())
                        }.onDelete { indexSet in
                            dataStore.flights.remove(atOffsets: indexSet)
                        }
                    }
                    .background {
                        Color(.white)
                    }
                    .scrollContentBackground(.hidden)
                }
                .navigationTitle("Flights")
                .toolbar {
                    ToolbarItem(placement: .navigationBarLeading) {
                        EditButton()
                    }
                }
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button("Add Flight") { isPresentingForm.toggle() }
                    }
                }
                .sheet(isPresented: $isPresentingForm) {
                    NavigationStack {
                        FlightForm(data: $newFlightFormData, presenting: $isPresentingForm)
                            .toolbar {
                                ToolbarItem(placement: .navigationBarLeading) {
                                    Button("Cancel") {
                                        flightLoader.reset()
                                        newFlightFormData = Flight.FormData()
                                        isPresentingForm = false
                                    }
                                }
                            }
                    }
                    .padding()
                }
            }
            ZStack {
                Divider()
            }
        }
    }
}

struct FlightView_Previews: PreviewProvider {
    static var previews: some View {
        FlightView()
            .environmentObject(DataStore())
            .environmentObject(FlightLoader(apiFlightClient: MockFlightAPIClient()))
    }
}
