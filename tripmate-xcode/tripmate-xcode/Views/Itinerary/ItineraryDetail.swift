import SwiftUI

struct ItineraryDetail: View {
    @EnvironmentObject var dataStore: DataStore
    @State var dateString: String
    
    var body: some View {
        VStack {
            Text(dateString)
                .font(.title)
                .bold()
            List(dataStore.findActivitiesForDate(dateString))
            { activity in
                HStack {
                    Text(activity.timeString)
                        .font(.title2)
                        .bold()
                    VStack(alignment: .leading) {
                        Text(activity.activityType.rawValue.uppercased())
                        Text(activity.title)
                            .font(.title2)
                            .bold()
                        HStack {
                            Image(systemName: "note.text")
                                .font(.title2)
                            Text(activity.notes)
                        }
                    }
                }
                .swipeActions(edge: .trailing) {
                    Button(role: .destructive) {
                        dataStore.removeActivity(activity)
                    } label: {
                        Label("Delete", systemImage: "trash")
                        
                    }
                }
            }
        }.padding([.top], -40)
    }
}

struct ItineraryDetail_Previews: PreviewProvider {
    static var previews: some View {
        ItineraryDetail(dateString: "Apr 8, 2023")
            .environmentObject(DataStore())
    }
}
