import Foundation

class SearchLoader: ObservableObject {
    let apiClient: TomTomAPI
    @Published private(set) var state: LoadingState = .idle
    
    enum LoadingState {
        case idle
        case loading
        case success(data: [Point])
        case failed(error: Error)
    }
    
    init(apiClient: TomTomAPI) {
        self.apiClient = apiClient
    }
    
    func reset() {
        self.state = .idle
    }
    
    @MainActor
    func loadTomTomData(type: String, latitude: Float, longitude: Float) async {
        let typeArr = type.components(separatedBy: " ")
        let typeStr = typeArr.joined(separator: "_")
        self.state = .loading
        do {
            let response: POIsResponse = try await apiClient.fetchPOIs(type: typeStr, latitude: latitude, longitude: longitude)
            self.state = .success(data: response.results)
        } catch {
            self.state = .failed(error: error)
        }
    }
}
