import Foundation

public struct POIsResponse: Decodable {
    var results: [Point]
    
    static func mock() -> POIsResponse {
        POIsResponse(results: [Point(id: "yE6tLH7jlKXBls8NHcUt2A", poi: POI(name: "Ocean Avenue"), address: Address(freeformAddress: "Ocean Avenue, Santa Monica, CA 90401"))])
    }
}

struct Point: Identifiable, Decodable {
    var id: String
    var poi: POI
    var address: Address
    
}

struct POI: Identifiable, Decodable {
    var id: String {
        return name
    }
    var name: String
    
}

struct Address: Identifiable, Decodable {
    var id: String {
        return freeformAddress
    }
    var freeformAddress: String
    
}
