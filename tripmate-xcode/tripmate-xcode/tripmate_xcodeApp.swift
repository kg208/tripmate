import SwiftUI

@main
struct tripmate_xcodeApp: App {
    init() {
        type = "";
        latitude = 34.0356;
        longitude = -118.5156;
        flightNumber = "UA875"
    }
    
    @StateObject var dataStore = DataStore()
    @StateObject var searchLoader = SearchLoader(apiClient: TomTomAPIClient())
    @StateObject var flightLoader = FlightLoader(apiFlightClient: FlightAPIClient())

    @State var type: String
    @State var latitude: Float
    @State var longitude: Float
    
    @State var flightNumber: String
    
    var body: some Scene {
        WindowGroup {
            TabContainer(type: $type, latitude: $latitude, longitude: $longitude)
                .environmentObject(dataStore)
                .environmentObject(searchLoader)
                .environmentObject(flightLoader)
        }
    }
}
