import SwiftUI

struct TabContainer: View {
    @EnvironmentObject var searchLoader: SearchLoader
    @EnvironmentObject var flightLoader: FlightLoader
    @Binding var type: String
    @Binding var latitude: Float
    @Binding var longitude: Float
    
    var body: some View {
        TabView{
            NavigationView {
                HomeView()
            }
            .tabItem {
                Label("Home", systemImage: "house")
            }
            NavigationView {
                FlightView()
            }
            .tabItem {
                Label("Flights", systemImage: "airplane")
            }
            NavigationView {
                ItineraryView(type: $type, latitude: $latitude, longitude: $longitude)
            }
            .tabItem {
                Label("Itinerary", systemImage: "checklist")
            }
            NavigationView {
                BudgetView()
            }
            .tabItem {
                Label("Budget", systemImage: "creditcard")
            }
        }
    }
}

struct TabContainer_Previews: PreviewProvider {
    static var previews: some View {
        TabContainer(type: Binding.constant("pizza"), latitude: Binding.constant(34.0356), longitude: Binding.constant(-118.5156))
            .environmentObject(DataStore())
            .environmentObject(SearchLoader(apiClient: MockTomTomAPIClient()))
            .environmentObject(FlightLoader(apiFlightClient: MockFlightAPIClient()))
    }
}
