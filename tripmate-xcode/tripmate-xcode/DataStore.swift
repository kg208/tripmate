import Foundation

class DataStore: ObservableObject {
    @Published var activities: [Itinerary] = Itinerary.previewData
    @Published var cities: [City] = City.previewData
    @Published var budgets: [Budget] = Budget.previewData
    @Published var flights: [Flight] = Flight.previewData
    @Published var startingBudget: StartingBudget = StartingBudget.previewData
    
    func addToActivities(_ itinerary: Itinerary) {
        activities.append(itinerary)
    }
    
    func findItineraryDates() -> [String] {
        var dates: [Date] = []
        var daysUsed: [String] = []
        for activity in activities {
            if !(daysUsed.contains(activity.dateString)) {
                dates.append(activity.date)
                daysUsed.append(activity.dateString)
            }
        }
        
        dates.sort(by: { $0.compare($1) == .orderedAscending })
        var datesString: [String] = []
        
        for date in dates {
            datesString.append(date.formatted(.dateTime.day().month().year()))
        }
        return datesString
    }
    
    func findActivitiesForDate(_ dateString: String) -> [Itinerary] {
        let specificActivities = activities
            .filter {
                activity in activity.dateString == dateString
            }
            .sorted {
                $0.time < $1.time
            }
        return specificActivities
    }
    
    func removeDate(at offsets: IndexSet) {
        var dateArr = findItineraryDates()
        dateArr.remove(atOffsets: offsets)
        var index = 0
        while (index < activities.count) {
            if !(dateArr.contains(activities[index].dateString)) {
                activities.remove(at: index)
            } else {
                index += 1
            }
        }
    }
    
    func removeActivity(_ itinerary: Itinerary) {
        if let index = activities.firstIndex(where: { $0.id == itinerary.id }) {
            activities.remove(at: index)
        }
    }
    
    func removeExpense(_ budget: Budget) {
        if let index = budgets.firstIndex(where: {$0.id == budget.id}) {
            budgets.remove(at:index)
        }
    }
    
    
    func addToBudget(_ budget: Budget) {
        budgets.append(budget)
    }
    
    func addStartingBudget(_ start: StartingBudget) {
        startingBudget = start
    }
    
    func updateStartingBudget() -> Float {
        return startingBudget.budgetNum ?? 0
    }
    
    func calculateRemainingBudget() -> Float {
        return (startingBudget.budgetNum ?? 0)  - calculateTotalSpent()
    }
    
    func calculateTotalFood() -> Float {
        var total = Float(0)
        for budget in budgets {
            if budget.budgetType == .food {
                if let cost = budget.domesticCostNum {
                    total += cost
                }
            }
        }
        return total
    }
    
    func calculateTotalAccomodations() -> Float {
        var total = Float(0)
        for budget in budgets {
            if budget.budgetType == .accomodation {
                if let cost = budget.domesticCostNum {
                    total += cost
                }
            }
        }
        return total
    }
    
    func calculateTotalFlights() -> Float {
        var total = Float(0)
        for budget in budgets {
            if budget.budgetType == .flight {
                if let cost = budget.domesticCostNum {
                    total += cost
                }
            }
        }
        return total
    }
    func calculateTotalTransport() -> Float {
        var total = Float(0)
        for budget in budgets {
            if budget.budgetType == .transportation {
                if let cost = budget.domesticCostNum {
                    total += cost
                }
            }
        }
        return total
    }
    
    func calculateTotalOther() -> Float {
        var total = Float(0)
        for budget in budgets {
            if budget.budgetType == .other {
                if let cost = budget.domesticCostNum {
                    total += cost
                }
            }
        }
        return total
    }
    
    func calculateTotalSpent() -> Float {
        var total = Float(0)
        for budget in budgets {
            if let cost = budget.domesticCostNum {
                total += cost
            }
        }
        return total
    }
    
    func addFlight(result: Result, flightNumber: String) {
        flights.append(Flight(flightNumber: flightNumber,
                              airline: result.response.data[0].airline.name,
                              origin: result.response.data[0].airport.origin.name,
                              originCode: result.response.data[0].airport.origin.code.iata,
                              destination: result.response.data[0].airport.destination.name,
                              destinationCode: result.response.data[0].airport.destination.code.iata,
                              scheduledDeparture: result.response.data[0].time.scheduled.departure,
                              scheduledArrival: result.response.data[0].time.scheduled.arrival
                             )
        )
    }
    
    func findHomeItinerary() -> [Itinerary] {
        let dateArr: [String] = findItineraryDates()
        return findActivitiesForDate(dateArr[0])
    }
    
    func findHomeDate() -> String {
        let dateArr: [String] = findItineraryDates()
        return dateArr[0]
    }
}

