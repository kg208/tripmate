import SwiftUI

struct FlightContainer: View {
    @State var origin: String
    @State var originCode: String
    @State var destination: String
    @State var destinationCode: String
    @State var scheduledDeparture: Int
    @State var scheduledArrival: Int
    
    var body: some View {
        HStack {
            FlightBox(airportName: origin, time: scheduledDeparture, code: originCode)
            Image(systemName: "airplane")
                .foregroundColor(Color(red: 0.363, green: 0.958, blue: 0.73))
            FlightBox(airportName: destination, time: scheduledArrival, code: destinationCode)
        }.padding()
    }
}

func getTime(time: Int) -> String {
    let date = Date(timeIntervalSince1970: Double(time))
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    let currentTime = dateFormatter.string(from: date)
    return currentTime
}

struct FlightBox: View {
    @State var airportName: String
    @State var time: Int
    @State var code: String
    
    var body: some View {
        VStack {
            Text(code)
                .font(.title2)
                .bold()
                .padding(.top, 10)
            Text(getTime(time: time))
            Text(Date(timeIntervalSince1970: Double(time)).formatted(.dateTime.day().month().year()))
            Text(airportName)
                .multilineTextAlignment(.center)
                .font(.caption)
                .padding(.bottom, 10)
                .padding(.top, 1)
                .padding(.leading, 10)
                .padding(.trailing, 10)
        }.frame(maxWidth:300, minHeight: 175)
            .background(Color(red: 0.949, green: 0.949, blue: 0.949))
            .cornerRadius(20)
    }
}

struct FlightContainer_Previews: PreviewProvider {
    static var previews: some View {
        FlightContainer(origin: "John F. Kennedy International Airport", originCode: "JFK", destination: "Raleigh-Durham International Airport", destinationCode: "RDU", scheduledDeparture: 1681407900,
                        scheduledArrival: 1681407900)
    }
}
