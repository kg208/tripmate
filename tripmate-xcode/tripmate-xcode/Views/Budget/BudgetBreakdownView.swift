import SwiftUI

struct BudgetBreakdownView: View {
    @EnvironmentObject var dataStore: DataStore
    var body: some View {
        VStack{
            Text("Expense Breakdown").font(.title)
            HStack{
                VStack{
                    Text("Flights")
                    Image(systemName: "airplane").padding(5)
                        .foregroundColor(Color(hue: 0.574, saturation: 0.559, brightness: 0.695))
                    Text("$" + String(dataStore.calculateTotalFlights()))
                }.padding()
                    .frame(maxWidth: 165)
                    .frame(maxHeight: 150)
                    .background(Color(red: 0.949, green: 0.949, blue: 0.949))
                    .cornerRadius(20)
                VStack{
                    Text("Accommodations")
                    Image(systemName: "bed.double.fill").padding(5)
                        .foregroundColor(Color(hue: 0.574, saturation: 0.559, brightness: 0.695))
                    Text("$" + String(dataStore.calculateTotalAccomodations()))
                }.padding()
                    .frame(maxWidth: 165)
                    .frame(maxHeight: 150)
                    .background(Color(red: 0.949, green: 0.949, blue: 0.949))
                    .cornerRadius(20)
            }
            HStack{
                VStack{
                    Text("Food")
                    Image(systemName: "fork.knife").padding(5)
                        .foregroundColor(Color(hue: 0.574, saturation: 0.559, brightness: 0.695))
                    Text("$" + String(dataStore.calculateTotalFood()))
                }.padding()
                    .frame(maxWidth: 165)
                    .frame(maxHeight: 150)
                    .background(Color(red: 0.949, green: 0.949, blue: 0.949))
                    .cornerRadius(20)
                VStack{
                    Text("Transportation")
                    Image(systemName: "car.fill").padding(5)
                        .foregroundColor(Color(hue: 0.574, saturation: 0.559, brightness: 0.695))
                    Text("$" + String(dataStore.calculateTotalTransport()))
                }.padding()
                    .frame(maxWidth: 165)
                    .frame(maxHeight: 150)
                    .background(Color(red: 0.949, green: 0.949, blue: 0.949))
                    .cornerRadius(20)
            }
            HStack{
                VStack{
                    Text("Other")
                    Image(systemName: "signpost.right.and.left.fill").padding(5)
                        .foregroundColor(Color(hue: 0.574, saturation: 0.559, brightness: 0.695))
                    Text("$" + String(dataStore.calculateTotalOther()))
                }.padding()
                    .frame(maxWidth: 165)
                    .frame(maxHeight: 150)
                    .background(Color(red: 0.949, green: 0.949, blue: 0.949))
                    .cornerRadius(20)
            }
            Spacer()
            ZStack {
                Divider()
            }
        }
        
    }
    
}

struct BudgetBreakdownView_Previews: PreviewProvider {
    static var previews: some View {
        BudgetBreakdownView().environmentObject(DataStore())
    }
}
