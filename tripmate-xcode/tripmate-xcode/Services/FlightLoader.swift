import Foundation

class FlightLoader: ObservableObject {
    let apiFlightClient: FlightAPI
    @Published private(set) var state: LoadingState = .idle
    
    enum LoadingState {
        case idle
        case loading
        case success(data: Result)
        case failed(error: Error)
    }
    
    init(apiFlightClient: FlightAPI) {
        self.apiFlightClient = apiFlightClient
    }
    
    func reset() {
        self.state = .idle
    }
    
    @MainActor
    func loadFlightData(flightNumber: String) async {
        self.state = .loading
        do {
            let responseFlight: FlightResponse = try await apiFlightClient.fetchFlight(flightNumber: flightNumber)
            self.state = .success(data: responseFlight.result)
        }
        catch {
            self.state = .failed(error: error)
        }
    }
}
