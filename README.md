# TripMate

## Description
TripMate is an app that helps users plan their trips!
Some of its features include:
- Flight information
- Top things to do near you
- Itinerary checklist
- Budget tracker


## iOS/API technologies
We're hoping to use MapKit to pin places user plans on going to.    
For our APIs, we're hoping to use the Flight Radar API from Rapid API to fetch and display flight information from user input. We are also hoping to use Amadeus to fetch tourist attractions and points of interest. However, this API requires access tokens, so we're not sure if this is feasible. Lastly, we are hoping to use an Exchange Rates Data API (https://apilayer.com/marketplace/exchangerates_data-api#pricing) in order to convert between the user's preferred currency and currency in another country.


## Complexity
- Fetching multiple APIs, one for flight info and another for attraction info
- Creating form to submit flight info
- Creating checklist where users input tasks and when checked will disappear
- Creating UI for budget tracking

## Jon Comment/Approval
Hey guys, looks great!  I've been working on a little travel app myself, so I am excited to steal ... um, be inspired by ... your ideas.

## Jon Storyboard Approval 3/26/23
