import Foundation

struct Itinerary: Identifiable {
    var id: UUID = UUID()
    var title: String
    var date: Date
    var dateString: String {
        date.formatted(.dateTime.day().month().year())
    }
    var timeString: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let currentTime = dateFormatter.string(from: date)
        return currentTime
    }
    var time: Int {
        let components = timeString.components(separatedBy: ":")
        let timeNums = components.joined(separator: "")
        if let time = Int(timeNums) {
            return time
        }
        return 0
    }
    var activityType: ActivityType
    var notes: String
    
    enum ActivityType: String, CaseIterable, Identifiable {
        var id: Self {self}
        case museum
        case nightlife
        case outdoors
        case shopping
        case show
        case tour
        case restaurant
    }
    
    struct FormData {
        var title: String = ""
        var date: Date = Date.now
        var activityType: ActivityType = .museum
        var notes: String = ""
    }
    
    var dataForForm: FormData {
        FormData(
            title: title,
            date: date,
            activityType: activityType,
            notes: notes
        )
    }
    
    static func create(from formData: FormData) -> Itinerary {
        let itinerary = Itinerary(title: formData.title, date: formData.date, activityType: formData.activityType, notes: formData.notes)
        return Itinerary.update(itinerary, from: formData)
    }
    
    static func update(_ itinerary: Itinerary, from formData: FormData) -> Itinerary {
        var itinerary = itinerary
        itinerary.title = formData.title
        itinerary.date = formData.date
        itinerary.activityType = formData.activityType
        itinerary.notes = formData.notes
        return itinerary
    }
}

extension Itinerary {
    static let previewData = [
        Itinerary(title: "Empire State Building", date: Date.now, activityType: .tour, notes: "Already purchased tickets"),
        Itinerary(title: "Katz's Deli", date: Date.now, activityType: .restaurant, notes: "Remember to bring cash!"),
        Itinerary(title: "Statue of Liberty", date: Date(timeIntervalSinceReferenceDate: 704117700), activityType: .tour, notes: "Need to catch the ferry at 10 AM"),
        Itinerary(title: "Museum of Modern Art", date: Date(timeIntervalSinceReferenceDate: 704217600), activityType: .museum, notes: "Already bought tickets"),
        Itinerary(title: "Metropolitan Museum of Art", date: Date(timeIntervalSinceReferenceDate: 704417700), activityType: .museum, notes: "Need to buy tickets")
    ]
}
