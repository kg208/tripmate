import SwiftUI

struct ItinerarySearchView: View {
    @EnvironmentObject var dataStore: DataStore
    @EnvironmentObject var searchLoader: SearchLoader
    @Binding var type: String
    @Binding var latitude: Float
    @Binding var longitude: Float
    @State var index: Int = 0
    
    func editLatLong(ind: Int) {
        latitude = dataStore.cities[index].latitude
        longitude = dataStore.cities[index].longitude
    }
    
    var body: some View {
        ScrollView {
            Picker(selection: $index, label: Text("City")) {
                ForEach(0..<dataStore.cities.count, id: \.self) {
                    Text(dataStore.cities[$0].name)
                }
            }
            .pickerStyle(.wheel)
            SearchFieldWithLabel(label: "Search", prompt: Text("Search"), text: $type, latitude: $latitude, longitude: $longitude, index: $index)
            HStack {
                Spacer()
                Button(action: {
                    editLatLong(ind: index)
                    Task { await searchLoader.loadTomTomData(type: type, latitude: latitude, longitude: longitude)
                    }
                }
                ) { Text("Search")
                }
                .buttonStyle(.borderedProminent)
                .disabled(type == "" ? true : false)
                Spacer()
            }
            switch searchLoader.state {
            case .idle: Color.clear
            case .loading: ProgressView()
            case .failed(let error): Text("Error \(error.localizedDescription)")
            case .success(let results):
                SearchDisplay(results: results)
            }
        }.onDisappear {
            searchLoader.reset()
            type = ""
        }
    }
}

struct ItinerarySearchView_Previews: PreviewProvider {
    static var previews: some View {
        ItinerarySearchView(type: Binding.constant(""), latitude: Binding.constant(34.0356), longitude: Binding.constant(-118.5156))
            .environmentObject(DataStore())
            .environmentObject(SearchLoader(apiClient: MockTomTomAPIClient()))
    }
}
