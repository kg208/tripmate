import Foundation

struct City: Identifiable {
  let id: Int
  let name: String
  let latitude: Float
  let longitude: Float
}

extension City {
  static let previewData = [
    City(id: 1, name: "Durham", latitude: 35.9940, longitude: -78.8986),
    City(id: 2, name: "New York", latitude: 40.7128, longitude: -74.0060),
    City(id: 3, name: "Pacific Palisades", latitude: 34.0481, longitude: -118.5256),
    City(id: 4, name: "Paris", latitude: 48.85661, longitude: 2.35222),
    City(id: 5, name: "Tahiti", latitude: -17.535000, longitude: -149.569595),
    City(id: 6, name: "Maldives", latitude: 1.924992, longitude: 73.399658)
  ]
}
