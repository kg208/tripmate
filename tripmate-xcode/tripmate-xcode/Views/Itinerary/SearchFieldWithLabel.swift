import SwiftUI

struct SearchFieldWithLabel: View {
    @EnvironmentObject var dataStore: DataStore
    @EnvironmentObject var searchLoader: SearchLoader
    @State var label: String
    @State var prompt: Text? = nil
    @Binding var text: String
    @Binding var latitude: Float
    @Binding var longitude: Float
    @Binding var index: Int
    
    func editLatLong(ind: Int) {
        latitude = dataStore.cities[index].latitude
        longitude = dataStore.cities[index].longitude
    }
    
    var body: some View {
        VStack(alignment: .leading) {
            TextField(label, text: $text, prompt: prompt != nil ? prompt : nil)
                .frame(height: 48)
                .padding(EdgeInsets(top: 0, leading: 6, bottom: 0, trailing: 6))
                .cornerRadius(10)
                .overlay(RoundedRectangle(cornerRadius: 10)
                    .stroke(lineWidth: 0.75)
                )
                .padding(.bottom, 20)
                .disableAutocorrection(true)
                .onSubmit {
                    if text != "" {
                        editLatLong(ind: index)
                        Task { await searchLoader.loadTomTomData(type: text, latitude: latitude, longitude: longitude)
                        }
                    }
                }
        }
        .padding([.leading, .trailing], 10)
    }
}

struct SearchFieldWithLabel_Previews: PreviewProvider {
    static var previews: some View {
        SearchFieldWithLabel(label: "Search", text: Binding.constant(""), latitude: Binding.constant(34.0356), longitude: Binding.constant(-118.5156), index: Binding.constant(0))
            .environmentObject(DataStore())
            .environmentObject(SearchLoader(apiClient: MockTomTomAPIClient()))
    }
}
