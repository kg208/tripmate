import SwiftUI

struct StartingBudgetForm: View {
    @Binding var data : StartingBudget.FormData
    
    var body: some View {
        Form {
            TextFieldWithLabel(label: "Starting budget in USD", text: $data.startingBudget, prompt: "ex. 1000")
        }
    }
}

struct StartingBudgetForm_Previews: PreviewProvider {
    static var previews: some View {
        StartingBudgetForm(data: Binding.constant(StartingBudget.FormData()))
    }
}
