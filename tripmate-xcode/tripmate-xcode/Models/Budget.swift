import Foundation

struct Budget: Identifiable {
    var id: UUID = UUID()
    var description: String
    var budgetType: BudgetType
    var foreignCost: String
    var conversion: String
    var conversionNum: Float? {
        Float(conversion)
    }
    var domesticCostNum: Float? {
        return Float(foreignCost)! / conversionNum!
    }
    
    enum BudgetType: String, CaseIterable, Identifiable {
        var id: Self {self}
        case flight
        case accomodation
        case food
        case transportation
        case other
    }
    
    struct FormData {
        var description: String = ""
        var budgetType: BudgetType = .flight
        var foreignCost: String = ""
        var conversion: String = ""
    }
    
    var dataForForm: FormData {
        FormData(
            description: description,
            budgetType: budgetType,
            foreignCost: foreignCost,
            conversion: conversion
        )
    }
    
    static func create(from formData: FormData) -> Budget {
        let budget = Budget(description: formData.description, budgetType: formData.budgetType, foreignCost: formData.foreignCost, conversion: formData.conversion)
        return Budget.update(budget, from: formData)
    }
    
    static func update(_ budget: Budget, from formData: FormData) -> Budget {
        var budget = budget
        budget.description = formData.description
        budget.budgetType = formData.budgetType
        budget.foreignCost = formData.foreignCost
        budget.conversion = formData.conversion
        return budget
    }
}

extension Budget {
    static let previewData = [
        Budget(description: "Sandwich from Katz's", budgetType: .food, foreignCost: "22", conversion: "2"),
        Budget(description: "Belt Bag", budgetType: .other, foreignCost: "40", conversion: "2"),
        Budget(description: "RDU to LGA", budgetType: .flight, foreignCost: "800", conversion: "2"),
        Budget(description: "Uber from Empire State", budgetType: .transportation, foreignCost: "12", conversion: "1"),
        Budget(description: "Night at Hilton", budgetType: .accomodation, foreignCost: "100", conversion: "1")
    ]
}

struct StartingBudget {
    var startingBudget: String
    var budgetNum: Float? {
        Float(startingBudget)
    }
    
    struct FormData {
        var startingBudget: String = ""
    }
    
    var dataforForm: FormData {
        FormData(startingBudget: startingBudget)
    }
    
    static func create(from formData: FormData) -> StartingBudget {
        let budget = StartingBudget(startingBudget: formData.startingBudget)
        return StartingBudget.update(budget, from: formData)
    }
    
    static func update(_ startingBudget: StartingBudget, from formData: FormData) -> StartingBudget {
        var startingBudget = startingBudget
        startingBudget.startingBudget = formData.startingBudget
        return startingBudget
    }
}

extension StartingBudget {
    static let previewData = StartingBudget(startingBudget: "1000")
}
