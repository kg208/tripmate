import SwiftUI

struct ItineraryView: View {
    @EnvironmentObject var dataStore: DataStore
    @State var isPresentingItineraryForm: Bool = false
    @State var newItineraryFormData = Itinerary.FormData()
    @EnvironmentObject var searchLoader: SearchLoader
    @Binding var type: String
    @Binding var latitude: Float
    @Binding var longitude: Float
    
    var body: some View {
        VStack {
            NavigationStack {
                NavigationLink(destination: ItinerarySearchView(type: $type, latitude: $latitude, longitude: $longitude)){
                    Text("Search For Activities")
                }
                .frame(maxWidth: 200)
                .frame(height: 30)
                .background(Color(red: 0.045, green: 0.207, blue: 0.455))
                .cornerRadius(30)
                .foregroundColor(.white)
                .padding(7)
                List {
                    ForEach(dataStore.findItineraryDates(), id: \.self)
                    { dateString in
                        NavigationLink(destination: ItineraryDetail(dateString: dateString)) {
                            Text(dateString)
                        }
                    }
                    .onDelete (perform: dataStore.removeDate)
                }
                .navigationTitle("Itinerary")
                .toolbar {
                    ToolbarItem(placement: .navigationBarLeading) {
                        EditButton()
                    }
                }
                .toolbar {
                    ToolbarItem(placement: .navigationBarTrailing) {
                        Button("Add") { isPresentingItineraryForm.toggle() }
                    }
                }
                .sheet(isPresented: $isPresentingItineraryForm) {
                    NavigationStack {
                        ItineraryForm(data: $newItineraryFormData)
                            .toolbar {
                                ToolbarItem(placement: .navigationBarLeading) {
                                    Button("Cancel") {
                                        isPresentingItineraryForm = false
                                        newItineraryFormData = Itinerary.FormData()
                                    }
                                }
                                ToolbarItem(placement: .navigationBarTrailing) {
                                    Button("Save") {
                                        let newItinerary = Itinerary.create(from: newItineraryFormData)
                                        dataStore.addToActivities(newItinerary)
                                        newItineraryFormData = Itinerary.FormData()
                                        isPresentingItineraryForm = false
                                    }
                                }
                            }
                    }
                    .padding()
                }
                ZStack {
                    Divider()
                }
            }
        }
    }
}

struct ItineraryView_Previews: PreviewProvider {
    static var previews: some View {
        ItineraryView(type: Binding.constant("pizza"), latitude: Binding.constant(34.0356), longitude: Binding.constant(-118.5156))
            .environmentObject(DataStore())
            .environmentObject(SearchLoader(apiClient: MockTomTomAPIClient()))
    }
}
