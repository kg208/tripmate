import SwiftUI

struct FlightForm: View {
    @Binding var data: Flight.FormData
    @Binding var presenting: Bool
    @EnvironmentObject var flightLoader: FlightLoader
    @EnvironmentObject var dataStore: DataStore
    
    var body: some View {
        VStack {
            TextFieldWithLabel(label: "Flight Number:", text: $data.flightNumber, prompt: "Ex. UA875")
                .padding(10)
            
            VStack {
                Button(action: {
                    Task { await flightLoader.loadFlightData(flightNumber: data.flightNumber)
                    }
                }
                ) { Text("Find Flight")
                }
                .buttonStyle(.borderedProminent)
            }
            
            switch flightLoader.state {
            case .idle: Color.clear
            case .loading: ProgressView()
            case .failed(let error):
                if checkError(error) {
                    Text("You have entered an invalid flight number. Please try again!")
                } else {
                    Text("Error \(error.localizedDescription)")
                }
            case .success(let result):
                
                FlightContainer(
                    origin:
                        result.response.data[0].airport.origin.name,
                    originCode:
                        result.response.data[0].airport.origin.code.iata,
                    destination: result.response.data[0].airport.destination.name,
                    destinationCode: result.response.data[0].airport.destination.code.iata,
                    scheduledDeparture: result.response.data[0].time.scheduled.departure,
                    scheduledArrival: result.response.data[0].time.scheduled.arrival)
                
                Button(action: {
                    dataStore.addFlight(result: result, flightNumber: data.flightNumber)
                    data = Flight.FormData()
                    flightLoader.reset()
                    presenting.toggle()
                }
                ) { Text("Add Flight")
                }
            }
            Spacer()
        }
    }
}

func checkError(_ error: Error) -> Bool {
    let errorStr = error.localizedDescription
    let errorArr = errorStr.components(separatedBy: " ")
    if errorArr[0] == "Decoding" {
        return true
    }
    return false
}

struct FlightForm_Previews: PreviewProvider {
    static var previews: some View {
        FlightForm(data: Binding.constant(Flight.FormData()), presenting: Binding.constant(false))
            .environmentObject(FlightLoader(apiFlightClient: MockFlightAPIClient()))
            .environmentObject(DataStore())
    }
}
